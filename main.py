# -+-+-+-+-+-+-+-+-+-+-+-+-+ #
#           Imports          #
# -+-+-+-+-+-+-+-+-+-+-+-+-+ #

from bs4 import BeautifulSoup
import requests

# -+-+-+-+-+-+-+-+-+-+-+-+-+ #
#          Functions         #
# -+-+-+-+-+-+-+-+-+-+-+-+-+ #

def ini():
    while True:
        decimal = input("Escreva o número de casas decimais que você deseja ver -> ")
        try:
            decimal = int(decimal)
            if decimal < 1:
                raise Exception("O número precisa ser positivo")
            else:
                break
        except ValueError:
            print("Valor inválido!\nInsira um número inteiro maior que 1")
            continue
    piDecimalPlaces = _getPiDecimals()
    print(f"Os números primos dentro das casas de pi escolhidas por você são:\n{__getPrime(piDecimalPlaces,decimal)}")

def _getPiDecimals() -> str:
    '''
    Faz uma request e recebe as primeiras 100.000 casas decimais de pi
    '''
    res = requests.get("http://www.geom.uiuc.edu/~huberty/math5337/groupe/digits.html")
    soup = BeautifulSoup(res.text,'html.parser')
    piDecimal = soup.body.text
    piDecimal = piDecimal.split("\n")
    piDecimal = "".join(piDecimal[2:-14])
    piDecimal = piDecimal[3:]
    return piDecimal

def __getPrime(pi:str,places:int):
    ret = []
    pi = pi[0:places]
    for e in pi:
        if _isPrime(int(e)):
            ret.append(e)
    return ",".join(ret)

def _isPrime(num:int)->bool:
    '''
    Retorna se o número passado é primo ou não
    '''
    if num <= 1:
        return False
    for i in range(2,num):
        if num % i == 0:
            return False
    return True

ini()